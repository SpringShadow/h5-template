import axios from 'axios';
import Vue from 'vue';
import {
	Message,
	Loading
} from 'element-ui';

import md5 from 'js-md5';
import {
	getMD5Staff
} from '../common/utils.js';

const config=require('../config/config.js')

axios.defaults.baseURL = config.baseUrl;
Vue.prototype.$axios = axios; // 在vue中使用axios

// loading框设置局部刷新，且所有请求完成后关闭loading框
let loading;
//声明一个对象用于存储请求个数
let needLoadingRequestCount = 0;

function startLoading() {
	loading = Loading.service({
		lock: true,
		fullscreen: true,
		background: 'rgba(0,0,0,0.7)'
	});
}

function endLoading() {
	loading.close();
}

function showFullScreenLoading() {
	if (needLoadingRequestCount === 0) {
		startLoading();
	}
	needLoadingRequestCount++;
}

function tryHideFullScreenLoading() {
	if (needLoadingRequestCount <= 0) return;
	needLoadingRequestCount--;
	if (needLoadingRequestCount === 0) {
		endLoading();
	}
}
// 把MD5加密的数据放在header里
function addMd5toHeader(request){
	let timestamp = (new Date()).getTime();
	let nonce = Math.random().toString(36).substring(2);
	request.headers.token = uni.getStorageSync('token');
	request.headers.timestamp = timestamp;
	request.headers.nonce = nonce;
	request.headers.signature = getMD5Staff(request.data, timestamp, nonce);
	return request
}
//请求拦截器
axios.interceptors.request.use((request) => {
	showFullScreenLoading();
	let newRequest=request;
	if(config.isMd5){
		newRequest=addMd5toHeader(request);
	}
	return newRequest;
}, (err) => {
	tryHideFullScreenLoading();
	Message.error({
		message: '请求超时!'
	});
	return Promise.reject(err);
});

axios.interceptors.response.use((res) => {
	tryHideFullScreenLoading();
	if (res.data.code == 200) {
		return res.data;
	} else if (res.data.code == 401) {
		Message.error({
			message: '请先登录'
		});
		return Promise.reject(res);
	} else if (res.data.code == 201) {
		Message.error({
			message: res.data.msg
		});
		return Promise.reject(res);
	}
	return res;
}, (err) => {
	tryHideFullScreenLoading();
	if (err.response.status == 504 || err.response.status == 404) {
		Message.error({
			message: '服务器被吃了⊙﹏⊙∥'
		});
	} else if (err.response.status == 403) {
		Message.error({
			message: '权限不足,请联系管理员!'
		});
	} else {
		Message.error({
			message: '未知错误'
		});
	}
	return Promise.reject(err);
});
