import Vue from 'vue';
import App from './App.vue';

const config = require('./config/config.js');

// 引入elementUi组件
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 移动端调试工具
import Vconsole from 'vconsole'
if(config.isVconsole){
	const vConsole = new Vconsole()
	Vue.use(vConsole)
}

// 引入路由和状态管理
import router from './router';
import store from './store';

// 引入api接口
import './api/api.js';

// 引入事件类
import Bus from 'common/bus'
Vue.prototype.Bus = Bus;

// 引入过滤器 
import filters from "./common/filter.js"
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
