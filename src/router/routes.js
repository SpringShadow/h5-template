const Index = () => import( /* webpackChunkName: "Index" */ '../views/index')
const Other = () => import( /* webpackChunkName: "Other" */ '../views/other');

export default [
	{
		path: '/',
		redirect: '/index'
	},
	{
		path: '/index',
		component: Index,
	},
	{
		path: '/other',
		component: Other,
	},
];
