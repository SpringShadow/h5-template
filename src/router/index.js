import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import routes from './routes';

export default new Router({
	routes: routes,
	// mode: 'history',
	linkActiveClass: 'active-link',
	linkExactActiveClass: 'exact-active-link',
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else {
			return {
				x: 0,
				y: 0
			};
		}
  }
});
