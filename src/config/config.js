let baseUrl = '';

if (process.env.NODE_ENV === 'production') {
	if (process.env.VUE_APP_FLAG === 'pro') {
		// 正式环境地址  
		baseURL = 'www.product.com';
	} else {
		// 测试环境地址
		baseURL = 'www.test.com';
	}
} else {
	// 本地开发环境地址
	baseUrl = 'http://localhost:8080' 
} 

let config = {
	// 是否需要MD5加密数据
	isMd5: false,
	// 请求接口的地址
	baseUrl:baseUrl,
	// 是否开启移动端vconsole
	isVconsole:false,
	// 打包部署的线上目录
	publicPath:'./'
}	

module.exports = config;
