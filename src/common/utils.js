import md5 from 'js-md5';
// js精确加法运算
export function accAdd(arg1, arg2) {
	var r1, r2, m, c;
	try {
		r1 = arg1.toString().split(".")[1].length;
	} catch (e) {
		r1 = 0;
	}
	try {
		r2 = arg2.toString().split(".")[1].length;
	} catch (e) {
		r2 = 0;
	}
	c = Math.abs(r1 - r2);
	m = Math.pow(10, Math.max(r1, r2));
	if (c > 0) {
		var cm = Math.pow(10, c);
		if (r1 > r2) {
			arg1 = Number(arg1.toString().replace(".", ""));
			arg2 = Number(arg2.toString().replace(".", "")) * cm;
		} else {
			arg1 = Number(arg1.toString().replace(".", "")) * cm;
			arg2 = Number(arg2.toString().replace(".", ""));
		}
	} else {
		arg1 = Number(arg1.toString().replace(".", ""));
		arg2 = Number(arg2.toString().replace(".", ""));
	}
	return (arg1 + arg2) / m;
};
// js精确乘法运算
export function floatMul(arg1, arg2) {
	var m = 0,
		s1 = arg1.toString(),
		s2 = arg2.toString();
	try {
		m += s1.split(".")[1].length
	} catch (e) {}
	try {
		m += s2.split(".")[1].length
	} catch (e) {}
	return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
};
// 处理要md5加密的数据
export function dictionaryOrderWithData(dic) {
	var result = "";
	var sdic = Object.keys(dic).sort(function(a, b) {
		return a.localeCompare(b)
	});
	var value = "";

	for (var ki in sdic) {
		if (dic[sdic[ki]] == null) {
			value = ""
		} else {
			if (typeof dic[sdic[ki]] == 'object') {
				value = JSON.stringify(dic[sdic[ki]]);
			} else {
				value = dic[sdic[ki]];
			}
		}
		result += sdic[ki] + value;
	}

	return result.replace(/\s/g, "");
};
// 获取到加密之后的数据
export function getMD5Staff(queryData, timestamp, nonce) {
	let data = '';
	if (typeof queryData == "object" && queryData instanceof Object) {
		data = dictionaryOrderWithData(queryData);
	}
	return md5(timestamp + nonce + data);
}
