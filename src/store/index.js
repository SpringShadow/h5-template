import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
		userid:'',
    },
    mutations: {
		setUserid(state,id){
			state.userid=id
		}
    }
})

export default store
