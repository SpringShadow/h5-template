const consoleTransformPlugin = require('babel-plugin-console-transform');

module.exports = {
	presets: [
		'@vue/app'
	],
	"plugins": [
		["@babel/plugin-transform-modules-commonjs", {
			"allowTopLevelThis": true
		}]
	]
}


