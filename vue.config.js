const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}
const config = require('./src/config/config.js');

module.exports = {
  devServer: {
    proxy: {
      '/api': {
		target: config.baseUrl, 
        changeOrigin: true,
        ws: true,
        '^/api': ''
      }
    }
  },
  chainWebpack: (config) => {
    config.resolve.alias
        .set('@', resolve('src'))
        .set('assets', resolve('src/assets'))
		.set('base', resolve('src/base'))
		.set('common', resolve('src/common'))
        .set('components', resolve('src/components'));
  },
  publicPath:process.env.NODE_ENV==='production'?`config.publicPath`:'/'
};
